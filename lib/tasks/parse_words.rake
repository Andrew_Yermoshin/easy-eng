require 'csv'

desc 'Parrsing words from the csv file'
task parse_words: :environment do
  p 'Clearing db'
  p '------------------------------------'

  Word.delete_all

  p 'Parsing the file'

  filename = File.open('words.csv')
  CSV.foreach(filename) do |line|
    Word.create(eng: line[0], rus: line[1])
    print 'Saved translation for '
    p line[0]
  end
end
