// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require bootstrap-sprockets
//= require turbolinks
//= require_tree .

$( document ).on('turbolinks:load', function () {
  function showTranslationOnBtnClick () {
    var showBtn = document.querySelector('.show-translation');
    var translationDiv = document.querySelector('.translation-div');

    showBtn.addEventListener('click', function () {
      translationDiv.classList.remove('hidden');
    });
  };

  function hideWordForAWeek () {
    var checkbox = document.querySelector('.checkbox input');
    var nextWordLink = document.querySelector('.next-word');
    var defaultNextWordLinkHref = nextWordLink.href;

    checkbox.addEventListener('change', function (e) {
      if (checkbox.checked) {
        var wordId = nextWordLink.dataset.wordId;
        nextWordLink.href = defaultNextWordLinkHref + '&hide=' + wordId;
      } else {
        nextWordLink.href = defaultNextWordLinkHref;
      }
    })
  };

  if (location.pathname !== '/') {
    showTranslationOnBtnClick();
    hideWordForAWeek();
  };
});
