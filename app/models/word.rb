class Word < ApplicationRecord
  validates :eng, uniqueness: true
  validates :eng, :rus, presence: true

  before_save :to_downcase

  def to_downcase
    self.eng = eng.downcase
  end
end
