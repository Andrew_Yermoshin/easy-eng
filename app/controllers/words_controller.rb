class WordsController < ApplicationController
  def index
    @word = Word.new
    @words = Word.order(updated_at: :desc)
    @next_word = Word.pluck(:id).sample
  end

  def create
    @word = Word.new(word_params)
    if @word.save
      redirect_to root_path
    else
      @words = Word.order(updated_at: :desc)
      render :index
    end
  end

  # we use sessions to help us not to show the same word too often
  def word
    if params.key?('hide')
      hide_word_for_a_week(params['hide'])
    end

    if session.fetch(:words, []).empty? || session[:lim] != params[:lim]
      word_ids = Word.where('hide_until < ? OR hide_until IS NULL', Time.current)
                     .order(created_at: :desc).pluck(:id)

      limit = params[:lim].to_i

      session[:words] = limit.positive? ? word_ids.first(limit) : word_ids
      session[:lim] = params[:lim]
    end

    word = session[:words].delete(session[:words].sample)

    @word = Word.find word
  end

  private

  def word_params
    params.require(:word).permit(:eng, :rus)
  end

  def hide_word_for_a_week(word_id)
    Word.find(word_id).update(hide_until: Time.current + 7.days)
  end
end
