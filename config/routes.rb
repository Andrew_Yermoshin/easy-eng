Rails.application.routes.draw do
  resources :words, only: %i[index create]
  get 'word', to: 'words#word'
  root 'words#index'
end
