class AddHideUntilToWords < ActiveRecord::Migration[5.1]
  def change
    add_column :words, :hide_until, :datetime
    add_index :words, :hide_until
  end
end
