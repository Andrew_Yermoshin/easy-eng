class AddUniqueIndexToWords < ActiveRecord::Migration[5.1]
  def change
    add_index :words, :eng, unique: true
  end
end
